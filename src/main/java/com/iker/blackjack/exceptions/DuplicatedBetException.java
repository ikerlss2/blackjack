package com.iker.blackjack.exceptions;

public class DuplicatedBetException extends RuntimeException {

    public DuplicatedBetException(long idUser) {
        super(String.format("The user %s has already bet in this game", idUser));
    }

}
