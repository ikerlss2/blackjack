package com.iker.blackjack.exceptions;

import com.iker.blackjack.model.currency.CurrencyType;

public class InsufficientCurrencyException extends RuntimeException {

    public InsufficientCurrencyException(long idUser, CurrencyType currencyType, int requiredAmount) {
        super(String.format("The user %s have less than %s %s", idUser, requiredAmount, currencyType));
    }

}
