package com.iker.blackjack.exceptions;

public class FullTableException extends RuntimeException {

    public FullTableException(long idUser) {
        super(String.format("The user %s cannot join the table because it is full", idUser));
    }

}
