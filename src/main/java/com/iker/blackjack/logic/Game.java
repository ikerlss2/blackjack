package com.iker.blackjack.logic;

import com.iker.blackjack.dto.BetRequest;
import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.exceptions.InsufficientCurrencyException;
import com.iker.blackjack.logic.interfaces.IDeckGenerator;
import com.iker.blackjack.logic.interfaces.IGame;
import com.iker.blackjack.logic.interfaces.IGameState;
import com.iker.blackjack.logic.states.*;
import com.iker.blackjack.model.configuration.TableConfiguration;
import lombok.Data;

@Data
public class Game implements IGame {

    private IGameState gameState;
    private final IDeckGenerator deckGenerator;
    private final GameComponents gameComponents;

    public Game(TableConfiguration tableConfiguration) {
        this.deckGenerator = new DeckGenerator();
        this.gameComponents = buildGameComponents(tableConfiguration);
    }

    private GameComponents buildGameComponents(TableConfiguration tableConfiguration) {
        var deck = deckGenerator.generateDeck(tableConfiguration.getDeckConfiguration());

        return new GameComponents(deck, tableConfiguration.getGameConfiguration(), new Dealer(), this::changeState);
    }

    public void changeState(GameStateType gameStateType) {
        switch (gameStateType) {
            case BET:
                gameState = new GameStateBet(gameComponents);
                break;

            case DEAL_CARDS:
                var thereIsSomePlayerWithBet = gameComponents.getPlayers().stream()
                        .anyMatch(player -> !player.getPlays().isEmpty());

                if (thereIsSomePlayerWithBet) {
                    gameState = new GameStateDealCards(gameComponents);
                }

                break;

            case PLAY_HAND:
                gameState = new GameStatePlayHand(gameComponents);
                break;

            case HAND_EVALUATION:
                gameState = new GameStateHandEvaluation(gameComponents);
                break;
        }

        /* We call the getter to make the tests easier and be able to mock the method */
        getGameState().start();
    }

    @Override
    public GameStateType getState() {
        return gameState != null ? gameState.getState() : null;
    }

    @Override
    public void start() {
        if (isStarted()) throw new IllegalStateException("The room is already initialized");
        changeState(GameStateType.BET);
    }

    @Override
    public void join(long idPlayer) {
        assert isStarted();
        /* We call the getter to make the tests easier and be able to mock the method */
        getGameState().join(idPlayer);
    }

    @Override
    public void left(long idPlayer) {
        assert isStarted();
        getGameState().left(idPlayer);
    }

    @Override
    public void bet(BetRequest betRequest) throws InsufficientCurrencyException {
        assert isStarted();
        getGameState().bet(betRequest);
    }

    @Override
    public void hit(PlayContext playContext) {
        assert isStarted();
        getGameState().hit(playContext);
    }

    @Override
    public void stand(PlayContext playContext) {
        assert isStarted();
        getGameState().stand(playContext);
    }

    @Override
    public void hitAndDouble(PlayContext playContext) {
        assert isStarted();
        getGameState().hitAndDouble(playContext);
    }

    public boolean isStarted() {
        return gameState != null;
    }

}
