package com.iker.blackjack.logic;

import com.iker.blackjack.exceptions.FullTableException;
import com.iker.blackjack.model.configuration.TableConfiguration;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Data
@Component
public class Table {

    private final Game game;
    private final TableConfiguration tableConfiguration;

    @Autowired
    public Table(TableConfiguration tableConfiguration) {
        this.tableConfiguration = tableConfiguration;
        this.game = new Game(tableConfiguration);
    }

    @PostConstruct
    public void startGame() {
        game.start();
    }

    public void join(long idPlayer) {
        if (game.getGameComponents().getPlayers().size() >= tableConfiguration.getMaxPlayersAmount())
            throw new FullTableException(idPlayer);

        game.join(idPlayer);
    }

}
