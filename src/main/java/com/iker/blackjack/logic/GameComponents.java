package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.IGameStateChanger;
import com.iker.blackjack.logic.interfaces.IHandEvaluator;
import com.iker.blackjack.model.Deck;
import com.iker.blackjack.model.Player;
import com.iker.blackjack.model.configuration.GameConfiguration;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class GameComponents {

    private Deck deck;
    private Dealer dealer;
    private List<Player> players;
    private IHandEvaluator handEvaluator;
    private IGameStateChanger stateChanger;
    private GameConfiguration gameConfiguration;
    private CardsShuffleMachine cardsShuffleMachine;

    public GameComponents(Deck deck, GameConfiguration gameConfiguration, Dealer dealer, IGameStateChanger stateChanger) {
        this.deck = deck;
        this.dealer = dealer;
        this.players = new ArrayList<>();
        this.handEvaluator = new HandEvaluator();
        this.stateChanger = stateChanger;
        this.gameConfiguration = gameConfiguration;
        this.cardsShuffleMachine = new CardsShuffleMachine();
    }

    public Optional<Player> findPlayerById(long idPlayer) {
        return players.stream()
                .filter(player -> player.getIdPlayer() == idPlayer)
                .findFirst();
    }

}
