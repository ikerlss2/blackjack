package com.iker.blackjack.logic.states;

import com.iker.blackjack.dto.BetRequest;
import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.exceptions.DuplicatedBetException;
import com.iker.blackjack.exceptions.InsufficientCurrencyException;
import com.iker.blackjack.logic.GameComponents;
import com.iker.blackjack.logic.interfaces.IGameState;
import com.iker.blackjack.manager.IWalletManager;
import com.iker.blackjack.manager.WalletManager;
import com.iker.blackjack.model.Play;
import com.iker.blackjack.model.Player;
import com.iker.blackjack.model.currency.CurrencyType;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Slf4j
public abstract class GameStateBase implements IGameState {

    protected ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    protected final IWalletManager walletManager;
    protected GameComponents gameComponents;
    protected GameStateType gameStateType;

    GameStateBase(GameComponents gameComponents, GameStateType gameStateType) {
        this.gameComponents = gameComponents;
        this.gameStateType = gameStateType;
        this.walletManager = new WalletManager();
    }

    GameStateBase(GameComponents gameComponents, GameStateType gameStateType, IWalletManager walletManager) {
        this.gameComponents = gameComponents;
        this.gameStateType = gameStateType;
        this.walletManager = walletManager;
    }

    @Override
    public void join(long idPlayer) {
        if (getState() != GameStateType.BET) throw new IllegalStateException("Nobody can join the game if the state is not in BET");

        var players = gameComponents.getPlayers();
        players.add(new Player(idPlayer));

        log.info("Player {} has join the game", idPlayer);
    }

    @Override
    public void left(long idPlayer) {
        var playerToLeft = gameComponents.findPlayerById(idPlayer)
                .orElseThrow(() -> new IllegalStateException(String.format("There is no player with id %s playing in this game", idPlayer)));
        var players = gameComponents.getPlayers();

        players.remove(playerToLeft);
        log.info("Player {} has left the game", idPlayer);

        if (players.isEmpty()) doFinish();
    }

    @Override
    public void bet(BetRequest betRequest) {
        if (getState() != GameStateType.BET) throw new IllegalStateException("Bet is not allowed in state " + getState());

        var idPlayer = betRequest.getIdPlayer();
        var player = gameComponents.findPlayerById(idPlayer)
                .orElseThrow(() -> new IllegalStateException(String.format("There is no player with id %s playing in this game", idPlayer)));

        checkIfPlayerCanBet(player, betRequest);
        walletManager.updateCurrency(idPlayer, CurrencyType.COINS, -betRequest.getBetAmount());

        player.addPlay(new Play(betRequest.getBetAmount()));
        log.info("Player {} has bet {} coins", betRequest.getIdPlayer(), betRequest.getBetAmount());
    }

    /**
     * These following methods cannot be implemented here since they need specific data of the state (PLAY_HAND state)
     */
    @Override
    public void hit(PlayContext playContext) {
        throwIllegalStateException();
    }

    @Override
    public void stand(PlayContext playContext) {
        throwIllegalStateException();
    }

    @Override
    public void hitAndDouble(PlayContext playContext) {
        throwIllegalStateException();
    }

    @Override
    public GameStateType getState() {
        return gameStateType;
    }

    @Override
    public void changeState(GameStateType gameStateType) {
        gameComponents.getStateChanger().changeState(gameStateType);
    }

    private void checkIfPlayerCanBet(Player player, BetRequest betRequest) {
        var idPlayer = player.getIdPlayer();

        if (player.hasAlreadyBet()) throw new DuplicatedBetException(idPlayer);
        if (!walletManager.hasEnoughCurrency(idPlayer, CurrencyType.COINS, betRequest.getBetAmount()))
            throw new InsufficientCurrencyException(idPlayer, CurrencyType.COINS, betRequest.getBetAmount());
    }

    protected void doFinish() {
        gameComponents.getDealer().setHand(null);
        var players = gameComponents.getPlayers();
        players.forEach(Player::cleanPlays);

        changeState(GameStateType.BET);
    }

    protected void showCurrentGameStatus() {
        log.info("Players: ");
        gameComponents.getPlayers().forEach(player -> log.info(player.toString()));

        log.info("Dealer: {} ", gameComponents.getDealer());
    }

    protected void throwIllegalStateException() {
        throw new IllegalStateException(String.format("Illegal method invocation for the state: %s", this.getClass().getSimpleName()));
    }

}
