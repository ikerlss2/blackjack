package com.iker.blackjack.logic.states;

import com.iker.blackjack.logic.GameComponents;
import com.iker.blackjack.model.Hand;
import com.iker.blackjack.model.interfaces.HandOwner;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;

@Slf4j
public class GameStateDealCards extends GameStateBase {

    public GameStateDealCards(GameComponents gameComponents) {
        super(gameComponents, GameStateType.DEAL_CARDS);
    }

    @Override
    public GameStateType getState() {
        return GameStateType.DEAL_CARDS;
    }

    @Override
    public void start() {
        shuffleCards();
        gameComponents.getPlayers().forEach(this::dealCards);
        dealCards(gameComponents.getDealer());

        showCurrentGameStatus();

        gameComponents.getStateChanger().changeState(GameStateType.PLAY_HAND);
    }

    public void dealCards(HandOwner player) {
        var card1 = gameComponents.getDeck().consumeNextCard();
        var card2 = gameComponents.getDeck().consumeNextCard();

        var hand = new Hand();
        hand.setCards(new ArrayList<>(Arrays.asList(card1, card2)));

        var handEvaluation = gameComponents.getHandEvaluator().evaluateHand(hand);

        player.setFirstHand(hand, handEvaluation);
    }

    public void shuffleCards() {
        gameComponents.getCardsShuffleMachine().shuffleDeck(gameComponents.getDeck());
    }

}
