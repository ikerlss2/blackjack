package com.iker.blackjack.logic.states;

import com.iker.blackjack.logic.GameComponents;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class GameStateBet extends GameStateBase {

    private final int secondsToBet;

    public GameStateBet(GameComponents gameComponents) {
        super(gameComponents, GameStateType.BET);
        this.secondsToBet = gameComponents.getGameConfiguration().getSecondsToBet();
    }

    @Override
    public GameStateType getState() {
        return GameStateType.BET;
    }

    @Override
    public void start() {
        log.info(String.format("The game will start in %s seconds", secondsToBet));
        scheduler.schedule(() -> gameComponents.getStateChanger().changeState(GameStateType.DEAL_CARDS), secondsToBet , TimeUnit.SECONDS);
    }

}
