package com.iker.blackjack.logic.states;

import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.exceptions.InsufficientCurrencyException;
import com.iker.blackjack.logic.GameComponents;
import com.iker.blackjack.model.Player;
import com.iker.blackjack.model.currency.CurrencyType;
import lombok.extern.slf4j.Slf4j;

import static com.iker.blackjack.logic.HandEvaluator.MAX_VALID_VALUE;
import static java.lang.Boolean.FALSE;

@Slf4j
public class GameStatePlayHand extends GameStateBase {

    private int currentPlayer;
    private int currentPlay;

    public GameStatePlayHand(GameComponents gameComponents) {
        super(gameComponents, GameStateType.PLAY_HAND);
    }

    @Override
    public GameStateType getState() {
        return GameStateType.PLAY_HAND;
    }

    @Override
    public void start() {
        log.info("Let's play your hands");

        currentPlayer = 0;
        currentPlay = 0;

        skipTurnIfNotPlayable();
    }

    protected void skipTurnIfNotPlayable() {
        var player = gameComponents.getPlayers().get(currentPlayer);

        if (!player.getPlays().isEmpty()) {
            var play = player.getPlays().get(currentPlay);
            if (play.isValidPlay() && !play.isFinishedPlay()) {
                return;
            }
        }

        nextTurn(player);
    }

    @Override
    public void hit(PlayContext playContext) {
        var player = gameComponents.getPlayers().get(currentPlayer);
        checkPlayerTurn(playContext, player);

        var play = player.getPlays().get(playContext.getPlayIndex());
        var hand = play.getHand();
        var card = gameComponents.getDeck().consumeNextCard();

        hand.addCard(card);

        var handEvaluation = gameComponents.getHandEvaluator().evaluateHand(hand);

        if (FALSE.equals(handEvaluation.getValid())) {
            play.setValidPlay(false);
            log.info("Player {} has lost his play {}", player.getIdPlayer(), playContext.getPlayIndex());
            nextTurn(player);

        } else if (handEvaluation.getMaxTotalValue() == MAX_VALID_VALUE) {
            play.setFinishedPlay(true);
            log.info("Player {} has 21 on his play {}", player.getIdPlayer(), playContext.getPlayIndex());
            nextTurn(player);
        }

        log.info("PLAY: {} = {}", playContext, play);
    }

    @Override
    public void stand(PlayContext playContext) {
        var player = gameComponents.getPlayers().get(currentPlayer);
        checkPlayerTurn(playContext, player);

        nextTurn(player);
    }

    @Override
    public void hitAndDouble(PlayContext playContext) {
        var player = gameComponents.getPlayers().get(currentPlayer);
        checkPlayerTurn(playContext, player);

        var play = player.getPlays().get(playContext.getPlayIndex());

        var bet = play.getBet();

        if (!walletManager.hasEnoughCurrency(player.getIdPlayer(), CurrencyType.COINS, bet))
            throw new InsufficientCurrencyException(player.getIdPlayer(), CurrencyType.COINS, bet);

        play.setFinishedPlay(true);
        play.incrementBet(bet);
        log.info("The bet of player {} is {}", player.getIdPlayer(), play.getBet());

        hit(playContext);
    }

    private void nextTurn(Player player) {
        if (player.getPlays().size() > (currentPlay + 1)) {
            currentPlay++;
        } else {
            currentPlay = 0;
            currentPlayer++;
            log.info("Player {} has ended his turn", player.getIdPlayer());
            if (allPlayersHasFinished()) {
                dealerPlay();
                gameComponents.getStateChanger().changeState(GameStateType.HAND_EVALUATION);
                return;
            }
        }

        skipTurnIfNotPlayable();
    }

    private void checkPlayerTurn(PlayContext playContext, Player player) {
        if (playContext.getIdPlayer() != player.getIdPlayer())
            throw new IllegalStateException(String.format("Not player turn exception. Current player turn is %s", player.getIdPlayer()));
        if (playContext.getPlayIndex() != currentPlay)
            throw new IllegalStateException(String.format("Not play turn. Play hand in order. Current play is %s", currentPlay));
    }

    private boolean allPlayersHasFinished() {
        return currentPlayer == gameComponents.getPlayers().size();
    }

    private void dealerPlay() {
        var dealer = gameComponents.getDealer();
        var dealerDecisionMaker = gameComponents.getDealer().getDealerDecisionMaker();
        var handEvaluator = gameComponents.getHandEvaluator();

        var haveToHit = dealerDecisionMaker.decideToHit(handEvaluator.evaluateHand(dealer.getHand()));

        while (haveToHit) {
            var card = gameComponents.getDeck().consumeNextCard();
            dealer.getHand().addCard(card);

            haveToHit =  dealerDecisionMaker.decideToHit(handEvaluator.evaluateHand(dealer.getHand()));
        }

        log.info("DEALER HAND: {}", dealer.getHand());
    }

}
