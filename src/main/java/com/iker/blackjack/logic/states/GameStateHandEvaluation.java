package com.iker.blackjack.logic.states;

import com.iker.blackjack.logic.GameComponents;
import com.iker.blackjack.model.ComparisonResult;
import com.iker.blackjack.model.HandEvaluation;
import com.iker.blackjack.model.Play;
import com.iker.blackjack.model.Player;
import com.iker.blackjack.model.currency.CurrencyType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameStateHandEvaluation extends GameStateBase {

    private static final float BET_WIN_MULTIPLIER = 2;
    private static final float BET_TIE_MULTIPLIER = 1;
    private static final float BET_BLACKJACK_WIN_MULTIPLIER = 2.5f;

    public GameStateHandEvaluation(GameComponents gameComponents) {
        super(gameComponents, GameStateType.HAND_EVALUATION);
    }

    @Override
    public GameStateType getState() {
        return GameStateType.HAND_EVALUATION;
    }

    @Override
    public void start() {
        var handEvaluator = gameComponents.getHandEvaluator();
        var dealerEvaluation = handEvaluator.evaluateHand(gameComponents.getDealer().getHand());

        gameComponents.getPlayers().forEach(player -> evaluatePlayerPlays(player, dealerEvaluation));

        doFinish();
    }

    private void evaluatePlayerPlays(Player player, HandEvaluation dealerHandEvaluation) {
        var handEvaluator = gameComponents.getHandEvaluator();

        player.getPlays().forEach(play -> {
            if (play.isValidPlay()) {
                var handEvaluation = handEvaluator.evaluateHand(play.getHand());
                var comparisonResult = handEvaluator.compareHandEvaluation(handEvaluation, dealerHandEvaluation);
                log.info("The player {} with play {} {}", player.getIdPlayer(), player.getPlays().indexOf(play), comparisonResult);

                grantPrize(player, play, handEvaluation, comparisonResult);
            }

        });
    }

    private void grantPrize(Player player, Play play, HandEvaluation handEvaluation, ComparisonResult comparisonResult) {
        var betRewardMultiplier = BET_TIE_MULTIPLIER;

        if (comparisonResult.equals(ComparisonResult.LOSE)) return;

        if (comparisonResult.equals(ComparisonResult.WIN)) {
            betRewardMultiplier = handEvaluation.getBlackjack().equals(Boolean.TRUE) ?
                    BET_BLACKJACK_WIN_MULTIPLIER : BET_WIN_MULTIPLIER;

        }

        walletManager.updateCurrency(player.getIdPlayer(), CurrencyType.COINS, play.getBet() * betRewardMultiplier);
    }

}
