package com.iker.blackjack.logic.states;

public enum GameStateType {
    BET,
    DEAL_CARDS,
    PLAY_HAND,
    HAND_EVALUATION
}
