package com.iker.blackjack.logic;

import com.iker.blackjack.model.Hand;
import com.iker.blackjack.model.HandEvaluation;
import com.iker.blackjack.model.interfaces.HandOwner;
import lombok.Data;

@Data
public class Dealer implements HandOwner {

    private Hand hand;
    private final DealerDecisionMaker dealerDecisionMaker = new DealerDecisionMaker();

    @Override
    public void setFirstHand(Hand hand, HandEvaluation handEvaluation) {
        this.hand = hand;
    }
}
