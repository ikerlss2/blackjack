package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.IHandEvaluator;
import com.iker.blackjack.model.*;
import com.iker.blackjack.model.interfaces.ISymbol;

import java.util.stream.Collectors;

import static com.iker.blackjack.model.SymbolTypes.ACE;

public class HandEvaluator implements IHandEvaluator {

    public static final int BLACKJACK_CARDS_AMOUNT = 2;
    public static final int MAX_VALID_VALUE = 21;

    @Override
    public ComparisonResult compareHandEvaluation(HandEvaluation target, HandEvaluation other) {
        var comparisonResult = target.compareTo(other);

        if (comparisonResult == 0) return ComparisonResult.TIE;
        return comparisonResult > 0 ? ComparisonResult.WIN : ComparisonResult.LOSE;
    }

    public HandEvaluation evaluateHand(Hand hand) {
        var symbols = hand.getCards().stream()
                .map(Card::getSymbol)
                .collect(Collectors.toList());
        var handValue = symbols.stream()
                .mapToInt(ISymbol::getValue)
                .sum();

        var isValid = isValid(handValue);

        if (!isValid) {
            var acesAmount = symbols.stream()
                    .filter(symbol -> symbol.equals(ACE))
                    .count();
            var aceMinToMaxValueDifference = ACE.getValue() - ACE.getMinValue();

            for (int i = 1; i <= acesAmount && !isValid; i++) {
                handValue -= aceMinToMaxValueDifference;
                isValid = isValid(handValue);
            }
        }

        var isBlackjack = isBlackjack(handValue, symbols.size());

        return new HandEvaluation(isValid, handValue, isBlackjack);
    }

    private boolean isValid(int value) {
        return value <= MAX_VALID_VALUE;
    }

    private boolean isBlackjack(int handValue, int symbolsAmount) {
        return handValue == MAX_VALID_VALUE && symbolsAmount == BLACKJACK_CARDS_AMOUNT;
    }

}
