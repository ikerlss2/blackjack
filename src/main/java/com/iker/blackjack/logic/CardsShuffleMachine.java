package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.ICardShuffler;
import com.iker.blackjack.model.Deck;

import java.util.Collections;

public class CardsShuffleMachine implements ICardShuffler {

    private static final int FIRS_CARD_POSITION = 0;

    @Override
    public void shuffleDeck(Deck deck) {
        Collections.shuffle(deck.getCards());
        deck.setNextCardPosition(FIRS_CARD_POSITION);
    }
}
