package com.iker.blackjack.logic;

import com.iker.blackjack.model.HandEvaluation;

public class DealerDecisionMaker {

    private static final int DEALER_VALUE_TO_STAND = 17;

    public boolean decideToHit(HandEvaluation handEvaluation) {
        return handEvaluation.getMaxTotalValue() < DEALER_VALUE_TO_STAND;
    }

}
