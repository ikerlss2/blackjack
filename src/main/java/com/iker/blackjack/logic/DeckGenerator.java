package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.IDeckGenerator;
import com.iker.blackjack.model.Card;
import com.iker.blackjack.model.Deck;
import com.iker.blackjack.model.Suit;
import com.iker.blackjack.model.configuration.DeckConfiguration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class DeckGenerator implements IDeckGenerator {

    @Override
    public Deck generateDeck(DeckConfiguration deckConfiguration) {
        var cards = new ArrayList<Card>();

        deckConfiguration.getSymbols().forEach(symbol ->
                Arrays.stream(Suit.values()).forEach(suit -> {
                    for (int i = 0; i < deckConfiguration.getDecksAmount(); i++) {
                        cards.add(new Card(suit, symbol));
                    }
                }));

         return new Deck(cards);
    }

}
