package com.iker.blackjack.logic.interfaces;

import com.iker.blackjack.model.Deck;
import com.iker.blackjack.model.configuration.DeckConfiguration;

public interface IDeckGenerator {

    Deck generateDeck(DeckConfiguration deckConfiguration);

}
