package com.iker.blackjack.logic.interfaces;

import com.iker.blackjack.dto.BetRequest;
import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.exceptions.DuplicatedBetException;
import com.iker.blackjack.exceptions.InsufficientCurrencyException;
import com.iker.blackjack.logic.states.GameStateType;

public interface IGame {

    void start();

    void join(long idPlayer);

    void left(long idPlayer);

    void bet(BetRequest betRequest) throws InsufficientCurrencyException, DuplicatedBetException;

    void hit(PlayContext playContext);

    void stand(PlayContext playContext);

    void hitAndDouble(PlayContext playContext) throws InsufficientCurrencyException, DuplicatedBetException;

    GameStateType getState();

    void changeState(GameStateType gameStateType);

}
