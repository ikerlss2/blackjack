package com.iker.blackjack.logic.interfaces;

import com.iker.blackjack.model.Deck;

public interface ICardShuffler {

    void shuffleDeck(Deck deck);

}
