package com.iker.blackjack.logic.interfaces;

import com.iker.blackjack.logic.states.GameStateType;

@FunctionalInterface
public interface IGameStateChanger {

    void changeState(GameStateType gameStateType);

}
