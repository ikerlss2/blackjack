package com.iker.blackjack.logic.interfaces;

import com.iker.blackjack.model.ComparisonResult;
import com.iker.blackjack.model.Hand;
import com.iker.blackjack.model.HandEvaluation;

public interface IHandEvaluator {

    HandEvaluation evaluateHand(Hand hand);

    ComparisonResult compareHandEvaluation(HandEvaluation target, HandEvaluation other);

}
