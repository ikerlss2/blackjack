package com.iker.blackjack.model.currency;

import lombok.Data;

@Data
public class GameCurrencyContainer {

    private final long amount;
    private final CurrencyType currencyType;

}
