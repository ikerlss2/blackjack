package com.iker.blackjack.model.currency;

public enum CurrencyType {
    COINS,
    CREDITS
}
