package com.iker.blackjack.model.configuration;

import lombok.Value;

@Value
public class TableConfiguration {

    int maxPlayersAmount;
    GameConfiguration gameConfiguration;
    DeckConfiguration deckConfiguration;

}
