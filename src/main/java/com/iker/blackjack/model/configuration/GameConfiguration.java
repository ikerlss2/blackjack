package com.iker.blackjack.model.configuration;

import lombok.Value;

@Value
public class GameConfiguration {

    int secondsToBet;

}
