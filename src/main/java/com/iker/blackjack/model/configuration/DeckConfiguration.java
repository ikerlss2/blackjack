package com.iker.blackjack.model.configuration;

import com.iker.blackjack.model.Symbol;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

import static com.iker.blackjack.model.SymbolTypes.*;

@Value
public class DeckConfiguration {

    int decksAmount = 2;
    List<Symbol> symbols = new ArrayList<>(List.of(ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JOKER, QUEEN, KING));

}
