package com.iker.blackjack.model.interfaces;

import com.iker.blackjack.model.Hand;
import com.iker.blackjack.model.HandEvaluation;

public interface HandOwner {

    void setFirstHand(Hand hand, HandEvaluation handEvaluation);

}
