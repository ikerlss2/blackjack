package com.iker.blackjack.model.interfaces;

public interface ISymbol {

    int getValue();

}
