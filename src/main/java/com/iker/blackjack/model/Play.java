package com.iker.blackjack.model;

import lombok.Data;

@Data
public class Play {

    private int bet;
    private Hand hand;
    private boolean validPlay;
    private boolean finishedPlay;

    public Play(int bet) {
        this.bet = bet;
        this.hand = new Hand();
        this.validPlay = true;
        this.finishedPlay = false;
    }

    public void setValidPlay(boolean validPlay) {
        this.validPlay = validPlay;
        if (!validPlay) {
            this.setFinishedPlay(true);
        }
    }

    public void incrementBet(int increment) {
        bet += increment;
    }

}
