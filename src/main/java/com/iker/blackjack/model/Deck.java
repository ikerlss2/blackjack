package com.iker.blackjack.model;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class Deck {

    @NonNull
    private List<Card> cards;
    private int nextCardPosition = 0;

    public Card consumeNextCard() {
        return cards.get(nextCardPosition++);
    }

}
