package com.iker.blackjack.model;

public class SymbolTypes {

    public static final SpecialSymbol ACE = new SpecialSymbol("ACE", 11, 1);
    public static final Symbol TWO = new Symbol("TWO", 2);
    public static final Symbol THREE = new Symbol("THREE", 3);
    public static final Symbol FOUR = new Symbol("FOUR", 4);
    public static final Symbol FIVE = new Symbol("FIVE", 5);
    public static final Symbol SIX = new Symbol("SIX", 6);
    public static final Symbol SEVEN = new Symbol("SEVEN", 7);
    public static final Symbol EIGHT = new Symbol("EIGHT", 8);
    public static final Symbol NINE = new Symbol("NINE", 9);
    public static final Symbol TEN = new Symbol("TEN", 10);
    public static final Symbol JOKER = new Symbol("JACK", 10);
    public static final Symbol QUEEN = new Symbol("QUEEN", 10);
    public static final Symbol KING = new Symbol("KING", 10);

}
