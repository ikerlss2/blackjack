package com.iker.blackjack.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hand {

    private List<Card> cards;

    public void addCard(Card card) {
        cards.add(card);
    }

}
