package com.iker.blackjack.model;

import lombok.Data;

@Data
public class HandEvaluation implements Comparable<HandEvaluation> {

    private final Boolean valid;
    private final int maxTotalValue;
    private final Boolean blackjack;

    @Override
    public int compareTo(HandEvaluation o) {
        var validResult = Boolean.compare(this.valid, o.valid);
        if (validResult != 0) return validResult;

        var valueResult = Integer.compare(this.maxTotalValue, o.maxTotalValue);

        return valueResult != 0 ? valueResult :
                        Boolean.compare(this.blackjack, o.blackjack);
    }
}
