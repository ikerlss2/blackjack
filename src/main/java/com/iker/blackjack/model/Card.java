package com.iker.blackjack.model;

import com.iker.blackjack.model.interfaces.ISymbol;
import lombok.Data;

@Data
public class Card {

    private final Suit suit;
    private final ISymbol symbol;

}
