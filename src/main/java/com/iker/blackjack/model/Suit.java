package com.iker.blackjack.model;

public enum Suit {

    CLUBS("BLACK"),
    DIAMONDS("RED"),
    HEARTS("RED"),
    SPADES("BLACK");

    private final String color;

    Suit(String color) {
        this.color = color;
    }
}
