package com.iker.blackjack.model;

import com.iker.blackjack.model.interfaces.ISymbol;

public class Symbol implements ISymbol {

    private final String name;
    private final int value;

    Symbol(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Symbol{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
