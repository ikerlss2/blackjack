package com.iker.blackjack.model;

import com.iker.blackjack.model.interfaces.HandOwner;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Player implements HandOwner {

    private final long idPlayer;
    private List<Play> plays = new ArrayList<>();

    public void cleanPlays() {
        plays = new ArrayList<>();
    }

    public void addPlay(Play play) {
        plays.add(play);
    }

    public boolean hasAlreadyBet() {
        return plays.stream()
                .anyMatch(play -> play.getBet() > 0);
    }

    public void setFirstHand(Hand hand, HandEvaluation handEvaluation) {
        plays.stream().findFirst()
                .ifPresent(play -> {
                    play.setHand(hand);
                    play.setFinishedPlay(handEvaluation.getBlackjack());
                });
    }

}
