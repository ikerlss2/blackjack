package com.iker.blackjack.model;

public enum ComparisonResult {
    LOSE,
    TIE,
    WIN
}
