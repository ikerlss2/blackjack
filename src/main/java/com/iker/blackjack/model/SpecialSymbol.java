package com.iker.blackjack.model;

public class SpecialSymbol extends Symbol {

    private final int minValue;

    SpecialSymbol(String name, int value, int minValue) {
        super(name, value);
        this.minValue = minValue;
    }

    public int getMinValue() {
        return minValue;
    }

}
