package com.iker.blackjack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayContext {

    private long idPlayer;
    private int playIndex;

}
