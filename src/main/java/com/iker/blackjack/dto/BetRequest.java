package com.iker.blackjack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BetRequest {

    private long idPlayer;
    private int betAmount;

}
