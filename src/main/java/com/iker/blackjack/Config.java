package com.iker.blackjack;

import com.iker.blackjack.model.configuration.DeckConfiguration;
import com.iker.blackjack.model.configuration.GameConfiguration;
import com.iker.blackjack.model.configuration.TableConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public TableConfiguration tableConfiguration() {
        return new TableConfiguration(3, new GameConfiguration(25), new DeckConfiguration());
    }

}
