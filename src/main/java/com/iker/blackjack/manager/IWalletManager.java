package com.iker.blackjack.manager;

import com.iker.blackjack.model.currency.CurrencyType;
import com.iker.blackjack.model.currency.GameCurrencyContainer;

public interface IWalletManager {

    long ANY_CURRENCY_AMOUNT = 1000;

    default GameCurrencyContainer getCurrencyContainer(long idUser, CurrencyType currencyType) {
         return new GameCurrencyContainer(ANY_CURRENCY_AMOUNT, currencyType);
    }

    default boolean hasEnoughCurrency(long idUser, CurrencyType currencyType, int requiredAmount) {
        var userCurrencyContainer = getCurrencyContainer(idUser, currencyType);

        return userCurrencyContainer.getAmount() >= requiredAmount;
    }

    default void updateCurrency(long idUser, CurrencyType currencyType, float amountToUpdate) {
        System.out.printf("Updating %s %s to user %s%n", amountToUpdate, currencyType, idUser);
    }

}
