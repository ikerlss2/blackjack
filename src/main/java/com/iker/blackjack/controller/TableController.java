package com.iker.blackjack.controller;

import com.iker.blackjack.logic.Table;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/table")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TableController {

    private final Table table;

    @PostMapping("/join")
    public void join(long idPlayer) {
        table.join(idPlayer);
    }

    @PostMapping("/left")
    public void left(long idPlayer) {
        table.getGame().left(idPlayer);
    }

}
