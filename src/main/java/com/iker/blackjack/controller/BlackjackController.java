package com.iker.blackjack.controller;

import com.iker.blackjack.dto.BetRequest;
import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.logic.Table;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/blackjack")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BlackjackController {

    private final Table table;

    @PostMapping("/bet")
    public void bet(BetRequest betRequest) {
        table.getGame().bet(betRequest);
    }

    @PostMapping("/hit")
    public void hit(PlayContext playContext) {
        table.getGame().hit(playContext);
    }

    @PostMapping("/stand")
    public void stand(PlayContext playContext) {
        table.getGame().stand(playContext);
    }

    @PostMapping("/hit-double")
    public void hitAndDouble(PlayContext playContext) {
        table.getGame().hitAndDouble(playContext);
    }

}
