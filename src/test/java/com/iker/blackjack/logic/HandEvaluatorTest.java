package com.iker.blackjack.logic;

import com.iker.blackjack.model.Card;
import com.iker.blackjack.model.ComparisonResult;
import com.iker.blackjack.model.Hand;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.iker.blackjack.model.SymbolTypes.*;
import static com.iker.blackjack.utils.Constants.ANY_SUIT;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class HandEvaluatorTest {

    HandEvaluator tested = new HandEvaluator();

    @Test
    void Given_Hand_When_EvaluateHand_Then_CorrespondingHandEvaluationIsReturned() {
        var card1 = new Card(ANY_SUIT, SIX);
        var card2 = new Card(ANY_SUIT, SEVEN);

        var hand = new Hand(List.of(card1, card2));

        var result = tested.evaluateHand(hand);

        assertThat(result)
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("maxTotalValue", SIX.getValue() + SEVEN.getValue())
                .hasFieldOrPropertyWithValue("blackjack", false);
    }

    @Test
    void Given_InvalidHand_When_EvaluateHand_Then_ReturnedHandEvaluationIsInvalid() {
        var card1 = new Card(ANY_SUIT, TEN);
        var card2 = new Card(ANY_SUIT, FIVE);
        var card3 = new Card(ANY_SUIT, KING);

        var hand = new Hand(List.of(card1, card2, card3));

        var result = tested.evaluateHand(hand);

        assertThat(result)
                .hasFieldOrPropertyWithValue("valid", false)
                .hasFieldOrPropertyWithValue("maxTotalValue", TEN.getValue() + FIVE.getValue() + KING.getValue())
                .hasFieldOrPropertyWithValue("blackjack", false);
    }

    @Test
    void Given_BlackjackHand_When_EvaluateHand_Then_ReturnedHandEvaluationIsInvalid() {
        var card1 = new Card(ANY_SUIT, JOKER);
        var card2 = new Card(ANY_SUIT, ACE);

        var hand = new Hand(List.of(card1, card2));

        var result = tested.evaluateHand(hand);

        assertThat(result)
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("maxTotalValue", JOKER.getValue() + ACE.getValue())
                .hasFieldOrPropertyWithValue("blackjack", true);
    }

    @Test
    void Given_21HandWith3Cards_When_EvaluateHand_Then_EvaluationIsNotBlackjack() {
        var card1 = new Card(ANY_SUIT, TEN);
        var card2 = new Card(ANY_SUIT, TEN);
        var card3 = new Card(ANY_SUIT, ACE);

        var hand = new Hand(List.of(card1, card2, card3));

        var result = tested.evaluateHand(hand);

        assertThat(result)
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("maxTotalValue", TEN.getValue() + TEN.getValue() + ACE.getMinValue())
                .hasFieldOrPropertyWithValue("blackjack", false);
    }

    @Test
    void Given_2ValidHands_When_CompareEvaluation_Then_WinHandWithHigherTotalValue() {
        var card1 = new Card(ANY_SUIT, TEN);
        var card2 = new Card(ANY_SUIT, NINE);

        var card3 = new Card(ANY_SUIT, TEN);
        var card4 = new Card(ANY_SUIT, SEVEN);

        var winnerHand = new Hand(List.of(card1, card2));
        var looserHand = new Hand(List.of(card3, card4));

        var winnerEvaluation = tested.evaluateHand(winnerHand);
        var looserEvaluation = tested.evaluateHand(looserHand);

        var result = tested.compareHandEvaluation(winnerEvaluation, looserEvaluation);

        assertThat(result).isEqualTo(ComparisonResult.WIN);
    }

    @Test
    void Given_2ValidHandsWithSameTotalValue_When_CompareEvaluation_Then_ComparisonResultIsTie() {
        var card1 = new Card(ANY_SUIT, TEN);
        var card2 = new Card(ANY_SUIT, NINE);

        var card3 = new Card(ANY_SUIT, TEN);
        var card4 = new Card(ANY_SUIT, NINE);

        var targetHand = new Hand(List.of(card1, card2));
        var otherHand = new Hand(List.of(card3, card4));

        var targetEvaluation = tested.evaluateHand(targetHand);
        var otherEvaluation = tested.evaluateHand(otherHand);

        var result = tested.compareHandEvaluation(targetEvaluation, otherEvaluation);

        assertThat(result).isEqualTo(ComparisonResult.TIE);
    }

    @Test
    void Given_2BlackjackHandVs21Hand_When_CompareEvaluation_Then_BlackjackHandWins() {
        var card1 = new Card(ANY_SUIT, JOKER);
        var card2 = new Card(ANY_SUIT, ACE);

        var card3 = new Card(ANY_SUIT, TEN);
        var card4 = new Card(ANY_SUIT, JOKER);
        var card5 = new Card(ANY_SUIT, ACE);

        var blackjackHand = new Hand(List.of(card1, card2));
        var otherHand = new Hand(List.of(card3, card4, card5));

        var blackjackEvaluation = tested.evaluateHand(blackjackHand);
        var otherEvaluation = tested.evaluateHand(otherHand);

        var result = tested.compareHandEvaluation(blackjackEvaluation, otherEvaluation);

        assertThat(result).isEqualTo(ComparisonResult.WIN);
    }

    @Test
    void Given_2BlackjackHands_When_CompareEvaluation_Then_ComparisonResultIsTie() {
        var card1 = new Card(ANY_SUIT, JOKER);
        var card2 = new Card(ANY_SUIT, ACE);

        var card3 = new Card(ANY_SUIT, KING);
        var card4 = new Card(ANY_SUIT, ACE);

        var targetHand = new Hand(List.of(card1, card2));
        var otherHand = new Hand(List.of(card3, card4));

        var targetEvaluation = tested.evaluateHand(targetHand);
        var otherEvaluation = tested.evaluateHand(otherHand);

        var result = tested.compareHandEvaluation(targetEvaluation, otherEvaluation);

        assertThat(result).isEqualTo(ComparisonResult.TIE);
    }

}
