package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.IHandEvaluator;
import com.iker.blackjack.model.Card;
import com.iker.blackjack.model.Hand;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.iker.blackjack.model.SymbolTypes.*;
import static com.iker.blackjack.utils.Constants.ANY_SUIT;
import static org.assertj.core.api.Assertions.assertThat;

class DealerDecisionMakerTest {

    IHandEvaluator handEvaluator = new HandEvaluator();
    DealerDecisionMaker tested = new DealerDecisionMaker();

    @Test
    void Given_HandWithValueLessThan17_When_DecideToHit_Then_HitActionTrueIsReturned() {
        var card1 = new Card(ANY_SUIT, SIX);
        var card2 = new Card(ANY_SUIT, TEN);
        var hand = new Hand(List.of(card1, card2));

        var result = tested.decideToHit(handEvaluator.evaluateHand(hand));

        assertThat(result).isTrue();
    }

    @Test
    void Given_HandWithValue17_When_EvaluateHand_Then_HitActionFalseIsReturned() {
        var card1 = new Card(ANY_SUIT, SEVEN);
        var card2 = new Card(ANY_SUIT, TEN);
        var hand = new Hand(List.of(card1, card2));

        var result = tested.decideToHit(handEvaluator.evaluateHand(hand));

        assertThat(result).isFalse();
    }

    @Test
    void Given_HandWithValueGreaterThan17_When_EvaluateHand_Then_HitActionFalseIsReturned() {
        var card1 = new Card(ANY_SUIT, KING);
        var card2 = new Card(ANY_SUIT, TEN);
        var hand = new Hand(List.of(card1, card2));

        var result = tested.decideToHit(handEvaluator.evaluateHand(hand));

        assertThat(result).isFalse();
    }

}
