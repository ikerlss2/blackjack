package com.iker.blackjack.logic;

import com.iker.blackjack.model.Deck;
import com.iker.blackjack.model.interfaces.ISymbol;
import com.iker.blackjack.model.Suit;
import com.iker.blackjack.model.configuration.DeckConfiguration;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.iker.blackjack.model.Suit.*;
import static com.iker.blackjack.model.SymbolTypes.*;
import static com.iker.blackjack.model.SymbolTypes.KING;
import static org.assertj.core.api.Assertions.assertThat;

class DeckGeneratorTest {

    static final DeckConfiguration ANY_DECK_CONFIGURATION = new DeckConfiguration();
    private static final int SUITS_AMOUNT = 4;

    DeckGenerator tested = new DeckGenerator();

    @Test
    void Given_DeckConfiguration_When_GenerateDeck_Then_DeckWithAllCardsIsGenerated() {
        var result = tested.generateDeck(ANY_DECK_CONFIGURATION);

        assertThat(result)
                .hasFieldOrPropertyWithValue("nextCardPosition", 0);
        assertThat(result.getCards())
                .size().isEqualTo(ANY_DECK_CONFIGURATION.getSymbols().size() * SUITS_AMOUNT * ANY_DECK_CONFIGURATION.getDecksAmount());
    }

    @Test
    void Given_DeckConfiguration_When_GenerateDeck_Then_EachSymbolHasCorrespondingSuits() {
        var result = tested.generateDeck(ANY_DECK_CONFIGURATION);
        var suitsPerSymbol = getSuitsPerSymbol(result);

        assertThat(suitsPerSymbol)
                .containsKeys(ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JOKER, QUEEN, KING);
        // For each symbol, we have to have all suits, and {decksAmount} repetitions of each one
        assertThat(suitsPerSymbol.values())
                .allMatch(suits -> {
                            Map<Suit, Long> suitFrequency = suits.stream()
                                    .collect(Collectors.groupingBy(Function.identity(),
                                            Collectors.counting()));

                            var decksAmount = ANY_DECK_CONFIGURATION.getDecksAmount();

                            return suitFrequency.get(CLUBS) == decksAmount &&
                                    suitFrequency.get(DIAMONDS) == decksAmount &&
                                    suitFrequency.get(HEARTS) == decksAmount &&
                                    suitFrequency.get(SPADES) == decksAmount;
                        }
                );

    }

    private HashMap<ISymbol, List<Suit>> getSuitsPerSymbol(Deck result) {
        var suitsPerSymbol = new HashMap<ISymbol, List<Suit>>();

        result.getCards().forEach(card -> {
            if (suitsPerSymbol.containsKey(card.getSymbol())) {
                var suitList = suitsPerSymbol.get(card.getSymbol());
                suitList.add(card.getSuit());
            } else {
                suitsPerSymbol.put(card.getSymbol(), new ArrayList<>(List.of(card.getSuit())));
            }
        });

        return suitsPerSymbol;
    }

}
