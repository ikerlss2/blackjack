package com.iker.blackjack.logic.states;

import com.iker.blackjack.exceptions.DuplicatedBetException;
import com.iker.blackjack.exceptions.InsufficientCurrencyException;
import com.iker.blackjack.logic.Dealer;
import com.iker.blackjack.logic.Game;
import com.iker.blackjack.manager.IWalletManager;
import com.iker.blackjack.model.*;
import com.iker.blackjack.model.configuration.DeckConfiguration;
import com.iker.blackjack.model.configuration.GameConfiguration;
import com.iker.blackjack.model.configuration.TableConfiguration;
import com.iker.blackjack.model.currency.CurrencyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.iker.blackjack.utils.Constants.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GameStateBaseTest {

    @Mock
    IWalletManager walletManager;

    GameStateBase tested;

    Game game = new Game(new TableConfiguration(3, new GameConfiguration(25), new DeckConfiguration()));

    @BeforeEach
    void beforeEach() {
        tested = spy(buildStateBase());
    }

    @Test
    void Given_NonBetState_When_Join_Then_IllegalStateExceptionIsThrown() {
        doReturn(GameStateType.HAND_EVALUATION).when(tested).getState();

        assertThatIllegalStateException()
                .isThrownBy(() -> tested.join(ANY_ID_PLAYER));
    }

    @Test
    void Given_BetState_When_Join_Then_ThePlayerJoinsTheGame() {
        tested.join(ANY_ID_PLAYER);

        assertThat(tested.gameComponents.getPlayers())
                .hasSize(1)
                .anyMatch(player -> player.getIdPlayer() == ANY_ID_PLAYER);
    }

    @Test
    void Given_AnyState_When_Left_Then_ThePlayerLeftsTheGame() {
        var players = new ArrayList<>(List.of(new Player(ANY_ID_PLAYER)));
        tested.gameComponents.setPlayers(players);

        tested.left(ANY_ID_PLAYER);

        assertThat(tested.gameComponents.getPlayers())
                .isEmpty();
    }

    @Test
    void Given_NonBetState_When_Bet_Then_IllegalStateExceptionIsThrown() {
        doReturn(GameStateType.HAND_EVALUATION).when(tested).getState();

        assertThatIllegalStateException()
                .isThrownBy(() -> tested.bet(ANY_BET_REQUEST));
    }

    @Test
    void Given_BetState_When_PlayerAlreadyBet_Then_DuplicatedBetExceptionIsThrown() {
        var player = mock(Player.class);
        var players = new ArrayList<>(List.of(player));

        when(player.hasAlreadyBet()).thenReturn(true);

        tested.gameComponents.setPlayers(players);

        assertThatExceptionOfType(DuplicatedBetException.class)
                .isThrownBy(() -> tested.bet(ANY_BET_REQUEST));
    }

    @Test
    void Given_BetState_When_PlayerTriesToBetMoreThanWhatHeOwns_Then_InsufficientCurrencyExceptionIsThrown() {
        var player = mock(Player.class);
        var players = new ArrayList<>(List.of(player));

        when(walletManager.hasEnoughCurrency(player.getIdPlayer(), CurrencyType.COINS, ANY_BET_REQUEST.getBetAmount()))
                .thenReturn(false);

        tested.gameComponents.setPlayers(players);

        assertThatExceptionOfType(InsufficientCurrencyException.class)
                .isThrownBy(() -> tested.bet(ANY_BET_REQUEST));
    }

    @Test
    void Given_BetState_When_EnoughCurrencyAndFirstBet_Then_BetIsDone() {
        var player = new Player(ANY_ID_PLAYER);
        var players = new ArrayList<>(List.of(player));
        tested.gameComponents.setPlayers(players);
        when(walletManager.hasEnoughCurrency(player.getIdPlayer(), CurrencyType.COINS, ANY_BET_REQUEST.getBetAmount()))
                .thenReturn(true);

        tested.bet(ANY_BET_REQUEST);

        var firstPlayOfPlayer = player.getPlays().get(0);
        assertThat(firstPlayOfPlayer.getBet())
                .isEqualTo(ANY_BET_REQUEST.getBetAmount());
    }

    @Test
    void Given_HandEvaluationState_When_DoFinish_Then_GameRelatedDataIsCleaned() {
        // Arrange
        var tested = spy(buildStateHandEvaluation());
        var player = new Player(ANY_ID_PLAYER);
        player.setPlays(List.of(new Play(50)));
        var players = new ArrayList<>(List.of(player));
        var dealer = new Dealer();
        dealer.setHand(new Hand(List.of(new Card(Suit.CLUBS, SymbolTypes.SEVEN), new Card(Suit.SPADES, SymbolTypes.NINE))));
        tested.gameComponents.setPlayers(players);
        tested.gameComponents.setDealer(dealer);

        // Assert previous state
        assertThat(tested.gameComponents.getPlayers())
                .allMatch(p -> p.getPlays().size() == 1);
        assertThat(tested.gameComponents.getDealer().getHand())
                .isNotNull();
        assertThat(tested.gameStateType).isEqualTo(GameStateType.HAND_EVALUATION);

        // Act
        tested.doFinish();

        // Assert
        assertThat(tested.gameComponents.getPlayers())
                .allMatch(p -> p.getPlays().isEmpty());
        assertThat(tested.gameComponents.getDealer().getHand())
                .isNull();
        verify(tested).changeState(GameStateType.BET);
    }

    @Test
    void Given_NonPlayHandState_When_Hit_Then_IllegalStateExceptionIsThrown() {
        assertThatIllegalStateException()
                .isThrownBy(() -> tested.hit(ANY_PLAY_CONTEXT));
    }

    @Test
    void Given_NonPlayHandState_When_Stand_Then_IllegalStateExceptionIsThrown() {
        assertThatIllegalStateException()
                .isThrownBy(() -> tested.stand(ANY_PLAY_CONTEXT));
    }

    @Test
    void Given_NonPlayHandState_When_HitAndDouble_Then_IllegalStateExceptionIsThrown() {
        assertThatIllegalStateException()
                .isThrownBy(() -> tested.hitAndDouble(ANY_PLAY_CONTEXT));
    }

    public GameStateBase buildStateBase() {
        return new GameStateBase(game.getGameComponents(), ANY_GAME_STATE_TYPE, walletManager) {
            @Override
            public void start() {

            }
        };
    }

    public GameStateBase buildStateHandEvaluation() {
        return new GameStateHandEvaluation(game.getGameComponents()) {
            @Override
            public void start() {

            }
        };
    }

}
