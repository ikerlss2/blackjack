package com.iker.blackjack.logic;

import com.iker.blackjack.logic.interfaces.IGameState;
import com.iker.blackjack.logic.states.GameStateType;
import com.iker.blackjack.model.configuration.DeckConfiguration;
import com.iker.blackjack.model.configuration.GameConfiguration;
import com.iker.blackjack.model.configuration.TableConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.iker.blackjack.utils.Constants.*;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GameTest {

    @Mock
    IGameState mockedGameState;

    Game tested = spy(new Game(new TableConfiguration(3, new GameConfiguration( 25), new DeckConfiguration())));

    @BeforeEach
    void beforeEach() {
        lenient().doReturn(mockedGameState).when(tested).getGameState();
        doReturn(true).when(tested).isStarted();
    }

    @Test
    void Given_NewGame_When_Start_Then_GameStateBetIsBuildAndStarted() {
        doReturn(false).when(tested).isStarted();

        tested.start();

        verify(tested).changeState(GameStateType.BET);
    }

    @Test
    void Given_StartedGame_When_Start_Then_AlreadyInitializedExceptionIsThrown() {
        doReturn(true).when(tested).isStarted();

        assertThatIllegalStateException()
                .isThrownBy(tested::start);
    }

    @Test
    void Given_Game_When_Join_Then_JoinIsPerformedByTheCurrentState() {
        tested.join(ANY_ID_PLAYER);

        verify(mockedGameState).join(ANY_ID_PLAYER);
    }

    @Test
    void Given_Game_When_Left_Then_JoinIsPerformedByTheCurrentState() {
        tested.left(ANY_ID_PLAYER);

        verify(mockedGameState).left(ANY_ID_PLAYER);
    }

    @Test
    void Given_Game_When_Bet_Then_JoinIsPerformedByTheCurrentState() {
        tested.bet(ANY_BET_REQUEST);

        verify(mockedGameState).bet(ANY_BET_REQUEST);
    }

    @Test
    void Given_Game_When_Hit_Then_JoinIsPerformedByTheCurrentState() {
        tested.hit(ANY_PLAY_CONTEXT);

        verify(mockedGameState).hit(ANY_PLAY_CONTEXT);
    }

    @Test
    void Given_Game_When_Stand_Then_JoinIsPerformedByTheCurrentState() {
        tested.stand(ANY_PLAY_CONTEXT);

        verify(mockedGameState).stand(ANY_PLAY_CONTEXT);
    }

    @Test
    void Given_Game_When_HitAndDouble_Then_JoinIsPerformedByTheCurrentState() {
        tested.hitAndDouble(ANY_PLAY_CONTEXT);

        verify(mockedGameState).hitAndDouble(ANY_PLAY_CONTEXT);
    }

    @Test
    void Given_Game_When_ChangeStateToOtherState_Then_NewStateIsStarted() {
        tested.changeState(GameStateType.PLAY_HAND);

        verify(mockedGameState).start();
    }

}
