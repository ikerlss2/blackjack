package com.iker.blackjack.utils;

import com.iker.blackjack.dto.BetRequest;
import com.iker.blackjack.dto.PlayContext;
import com.iker.blackjack.logic.states.GameStateType;
import com.iker.blackjack.model.Suit;

public class Constants {

    public static final long ANY_ID_PLAYER = 0;
    public static final int ANY_BET_AMOUNT = 50;
    public static final int ANY_PLAY_INDEX = 50;
    public static final Suit ANY_SUIT = Suit.CLUBS;
    public static final GameStateType ANY_GAME_STATE_TYPE = GameStateType.BET;
    public static final BetRequest ANY_BET_REQUEST = new BetRequest(ANY_ID_PLAYER, ANY_BET_AMOUNT);
    public static final PlayContext ANY_PLAY_CONTEXT = new PlayContext(ANY_ID_PLAYER, ANY_PLAY_INDEX);

}
